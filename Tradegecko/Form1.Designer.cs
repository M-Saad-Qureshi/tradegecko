﻿namespace Tradegecko
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new System.Windows.Forms.Button();
            this.txtBxAPIToken = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.btnSelect = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnInsertNew = new System.Windows.Forms.Button();
            this.txtBxFileName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.grpBxData = new System.Windows.Forms.GroupBox();
            this.btnUpload = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.grpBxData.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(337, 110);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(155, 23);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Get Orders";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // txtBxAPIToken
            // 
            this.txtBxAPIToken.Location = new System.Drawing.Point(162, 68);
            this.txtBxAPIToken.Name = "txtBxAPIToken";
            this.txtBxAPIToken.Size = new System.Drawing.Size(627, 20);
            this.txtBxAPIToken.TabIndex = 5;
            this.txtBxAPIToken.Text = "899a23262a5f8b28de6456b73991732fe86ac0a6bd61b1922dbf16d100443966";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(92, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "API Token :";
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Location = new System.Drawing.Point(6, 89);
            this.dgvData.Name = "dgvData";
            this.dgvData.ReadOnly = true;
            this.dgvData.Size = new System.Drawing.Size(818, 364);
            this.dgvData.TabIndex = 14;
            this.dgvData.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellClick);
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(116, 42);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(608, 23);
            this.btnSelect.TabIndex = 15;
            this.btnSelect.Text = "Get \"unpacked\" and \"active\" orders";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnInsertNew);
            this.groupBox1.Controls.Add(this.txtBxFileName);
            this.groupBox1.Controls.Add(this.btnStart);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtBxAPIToken);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(830, 149);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "File option";
            // 
            // btnInsertNew
            // 
            this.btnInsertNew.Location = new System.Drawing.Point(675, 120);
            this.btnInsertNew.Name = "btnInsertNew";
            this.btnInsertNew.Size = new System.Drawing.Size(155, 23);
            this.btnInsertNew.TabIndex = 22;
            this.btnInsertNew.Text = "Insert Prototype";
            this.btnInsertNew.UseVisualStyleBackColor = true;
            this.btnInsertNew.Click += new System.EventHandler(this.btnInsertNew_Click);
            // 
            // txtBxFileName
            // 
            this.txtBxFileName.Location = new System.Drawing.Point(162, 28);
            this.txtBxFileName.Name = "txtBxFileName";
            this.txtBxFileName.Size = new System.Drawing.Size(627, 20);
            this.txtBxFileName.TabIndex = 21;
            this.txtBxFileName.TextChanged += new System.EventHandler(this.txtBxFileName_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(143, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Spread-sheet name to save :";
            // 
            // grpBxData
            // 
            this.grpBxData.Controls.Add(this.btnUpload);
            this.grpBxData.Controls.Add(this.dgvData);
            this.grpBxData.Controls.Add(this.btnSelect);
            this.grpBxData.Enabled = false;
            this.grpBxData.Location = new System.Drawing.Point(13, 168);
            this.grpBxData.Name = "grpBxData";
            this.grpBxData.Size = new System.Drawing.Size(830, 509);
            this.grpBxData.TabIndex = 22;
            this.grpBxData.TabStop = false;
            this.grpBxData.Text = "Data options";
            // 
            // btnUpload
            // 
            this.btnUpload.Location = new System.Drawing.Point(380, 468);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(75, 23);
            this.btnUpload.TabIndex = 18;
            this.btnUpload.Text = "Upload";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 682);
            this.Controls.Add(this.grpBxData);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Trade Gecko";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpBxData.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TextBox txtBxAPIToken;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtBxFileName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox grpBxData;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Button btnInsertNew;
    }
}

