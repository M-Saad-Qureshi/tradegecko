﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tradegecko
{
    class Class2
    {
        public class OrderLineItem : PostOrderLineItem
        {
            public int id { get; set; }
            public DateTime created_at { get; set; }
            public DateTime updated_at { get; set; }
            public int order_id { get; set; }
            public int tax_type_id { get; set; }
            public string image_url { get; set; }
            public object label { get; set; }
            public object line_type { get; set; }
            public int position { get; set; }
            public object tax_rate { get; set; }
            public IList<int> fulfillment_line_item_ids { get; set; }
            public IList<object> fulfillment_return_line_item_ids { get; set; }
            public IList<int> invoice_line_item_ids { get; set; }
        }

        public class PostOrderLineItem
        {
            public int variant_id { get; set; }
            public int price { get; set; }
            public int quantity { get; set; }
            public int discount { get; set; }
            public object freeform { get; set; }
            public int tax_rate_override { get; set; }
        }

        public class OrderLineItemListResponse
        {
            public IList<OrderLineItem> order_line_items { get; set; }
        }
    }
}
