﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Tradegecko
{
    public partial class Product_Form : Form
    {
        CookieContainer cookieJar = new CookieContainer();

        // AccessToken of my tradegecko account 

        String apiToken = "";
        String authorizeURL = "https://api.tradegecko.com/accounts";
        String productAPIURL = "https://api.tradegecko.com/products/";
      

       // String updateOrderAPIURL = "https://api.tradegecko.com/order/";

        Class1.APIResponse orders;

        public Product_Form()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            Boolean allValid = GetUIInputs();

            if (allValid == false)
            {
                MessageBox.Show("Please fill API Token", "Problem", MessageBoxButtons.OK);
                return;
            }


            HttpWebRequest webReq;
            String response;

            webReq = HTTPHelper.getWebRequestTradegecko(authorizeURL, apiToken);
            response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "GET", "", true);

           // string productJson = //JSONHelper.serializeOrder(collectionWrapper);

            // Creating product in gecko   
            webReq = HTTPHelper.getWebRequestTradegecko(productAPIURL, apiToken);
           // response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "POST", productJson, true);
        }

        /// <summary>
        /// It collects UI inputs
        /// </summary>
        /// <returns>All Inputs are valid or not</returns>
        private Boolean GetUIInputs()
        {
            Boolean allValid = false;
            try
            {
                if (String.IsNullOrEmpty(txtBxAPIToken.Text) == false)
                {
                    apiToken = txtBxAPIToken.Text;
                    allValid = true;
                }

                else
                {
                    allValid = false;
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }

            return allValid;
        }

        private string getProductJSON()
        {
            string json = "";
            try
            {
               
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }

            return json;
        }
    }
}
