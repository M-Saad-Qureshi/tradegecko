﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tradegecko
{
    class JSONHelper
    {
        public static Class5.ProductItemResponse deserializProductItem(String response)
        {
            return JsonConvert.DeserializeObject<Class5.ProductItemResponse>(response);
        }

        public static Class4.VariantItemReponse deserializeVariantItem(String response)
        {
            return JsonConvert.DeserializeObject<Class4.VariantItemReponse>(response);
        }

        public static Class1.APIResponse deserializeObject(String response)
        {
           return JsonConvert.DeserializeObject<Class1.APIResponse>(response);
        }

        public static Class2.OrderLineItemListResponse deserializeOrderLineItemList(String response)
        {
            return JsonConvert.DeserializeObject<Class2.OrderLineItemListResponse>(response);
        }

        public static Class3.OrderLineItemReponse deserializeOrderLineItem(String response)
        {
            return JsonConvert.DeserializeObject<Class3.OrderLineItemReponse>(response);
        }

        public static String serializeAPIResponse(Class1.APIResponse response)
        {
            return JsonConvert.SerializeObject(response);
        }

        public static String serializeOrder(Class1.Order order)
        {
            return JsonConvert.SerializeObject(order);
        }
        public static String serializeOrder(List<Class1.Order> order)
        {
            return JsonConvert.SerializeObject(order);
        }

        public static String serializeOrder(dynamic order)
        {
            return JsonConvert.SerializeObject(order);
        }

    }
}
