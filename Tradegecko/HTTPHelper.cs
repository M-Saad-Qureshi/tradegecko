﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Tradegecko
{
    class HTTPHelper
    {
        private static String USER_AGENT_MOZILLA = "Mozilla/5.0 (Windows NT 5.1; rv:32.0) Gecko/20100101 Firefox/32.0";

        public static HttpWebRequest getWebRequestTradegecko(String url, string apiToken)
        {
            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(url);

            //    webReq.Accept = "application/vnd.tradegecko.v1+json";

            webReq.Accept = "application/json";

            webReq.Headers.Add("Authorization", "Bearer " + apiToken);

            return webReq;
        }

        public static string GetResponse(HttpWebRequest webReq,
                                                    CookieContainer cookieJar,
                                                    string Referer,
                                                    Boolean UseProxy,
                                                    string Method,
                                                    string Data,
                                                    bool allowAutoRedirect,
                                                    string contentType = "application/json")
        {
            string htmlResponse = string.Empty;

            try
            {
                webReq.CookieContainer = cookieJar;

                webReq.ReadWriteTimeout = 15000; // it is 15 seconds

                if (String.IsNullOrEmpty(webReq.Accept) == true)
                {
                    webReq.Accept = "image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/x-shockwave-flash, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*";
                }

                webReq.AutomaticDecompression = DecompressionMethods.Deflate;

                webReq.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-us");

                webReq.KeepAlive = true;

                webReq.ServicePoint.Expect100Continue = false;

                if (UseProxy)
                {
                    //  WebProxy myproxy = new WebProxy(ip, port);
                    //  myproxy.BypassProxyOnLocal = false;
                    //  webReq.Proxy = myproxy;
                }

                webReq.ProtocolVersion = HttpVersion.Version11;

                webReq.AllowAutoRedirect = allowAutoRedirect;

                if (!string.IsNullOrEmpty(Referer))
                {
                    webReq.Referer = Referer;
                }

                webReq.UserAgent = USER_AGENT_MOZILLA;

                if (Method.ToLower() == "post" || Method.ToLower() == "put")
                {
                    if (string.IsNullOrEmpty(Data) == false)
                    {
                        //webReq.ContentLength = ASCIIEncoding.ASCII.GetBytes(Data).Length;

                        webReq.Method = Method.ToUpper();

                        var data = ASCIIEncoding.ASCII.GetBytes(Data);

                        byte[] bytes = System.Text.Encoding.ASCII.GetBytes(Data);
                        webReq.ContentLength = bytes.Length;
                        webReq.ContentType = contentType;
                        Stream stream = webReq.GetRequestStream();
                        stream.Write(bytes, 0, bytes.Length); //Push it out there
                        stream.Close();

                        //using (var stream = webReq.GetRequestStream())
                        //{
                        //    stream.Write(data, 0, data.Length);
                        //}
                    }
                }

                HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();

                int cookieCount = cookieJar.Count;

                Stream s = webResponse.GetResponseStream();
                using (StreamReader sr = new StreamReader(s))
                {
                    s.Flush();
                    htmlResponse = sr.ReadToEnd();

                }
            }


            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    WebResponse resp = e.Response;

                    if (String.IsNullOrEmpty(resp.Headers.Get("X-Request-Id")) == false)
                    {
                        System.Console.WriteLine(resp.Headers.Get("X-Request-Id"));
                    }

                    using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                    {
                        htmlResponse = (sr.ReadToEnd());
                    }
                }

                if (e.Status == WebExceptionStatus.NameResolutionFailure)
                {
                    WebResponse resp = e.Response;
                    using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                    {
                        htmlResponse = (sr.ReadToEnd());
                    }
                }

                if (e.Status == WebExceptionStatus.ServerProtocolViolation)
                {
                 
                }
            }

            return htmlResponse;
        }
    }
}
