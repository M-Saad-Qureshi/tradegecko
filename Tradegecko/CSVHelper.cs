﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Tradegecko
{
    class CSVHelper
    {
        public static List<Class1.Order> readCSV(string fileName)
        {
            List<Class1.Order> records = new List<Class1.Order>();
            fileName += ".csv";

            if (File.Exists(fileName))
            {
                StreamReader sr = null;
                sr = new StreamReader(fileName);
                var csv = new CsvReader(sr);
                records = csv.GetRecords<Class1.Order>().ToList();
                sr.Close();
                return records;
            }

            return records;           
        }

        public static void writeCSV(IList<Class1.Order> customList, Boolean AppendText)
        {
            string fileName = DateTime.Now.ToString();

            fileName = fileName.Replace(':', ';');
            fileName += ".csv";
            string filePath = Environment.CurrentDirectory + "\\" + fileName;
            StreamWriter sw = null;

            try
            {
                sw = new StreamWriter(filePath, AppendText);
                var csv = new CsvWriter(sw);
                csv.WriteRecords(customList);
            }


            catch (IOException ex)
            {
                System.Console.WriteLine(ex.Message);
                filePath = Environment.CurrentDirectory + "\\" + fileName;
                filePath += "(2)";
            }

            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
            }
        }

        public static void writeCSV(string fileName, Class1.Order order, Boolean AppendText)
        {
            fileName += ".csv";
            StreamWriter sw = null;

            try
            {
                sw = new StreamWriter(fileName, AppendText);
                var csv = new CsvWriter(sw);
                csv.WriteRecord(order);
            }


            catch (IOException ex)
            {
                System.Console.WriteLine(ex.Message);
                fileName = Environment.CurrentDirectory + "\\" + fileName;
                fileName += "(2)";
            }

            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
            }
        }

        public static void writeCSV(string fileName,List<VBASheet> vbaSheetList, Boolean AppendText)
        {
            fileName += ".csv";
            StreamWriter sw = null;

            try
            {
                sw = new StreamWriter(fileName, AppendText);
                var csv = new CsvWriter(sw);
                csv.WriteRecords(vbaSheetList);
            }


            catch (IOException ex)
            {
                System.Console.WriteLine(ex.Message);
                fileName = Environment.CurrentDirectory + "\\" + fileName;
                fileName += "(2)";
            }

            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
            }
        }

        public static void writeCSVHeaders(string fileName, Boolean AppendText)
        {
            fileName += ".csv";
            StreamWriter sw = null;

            try
            {
                List<Class1.Order> orderList = new List<Class1.Order>();


                sw = new StreamWriter(fileName, AppendText);
                var csv = new CsvWriter(sw);
                csv.WriteRecords(orderList);
            }


            catch (IOException ex)
            {
                System.Console.WriteLine(ex.Message);
                fileName = Environment.CurrentDirectory + "\\" + fileName;
                fileName += "(2)";
            }

            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
            }
        }
    }
}
