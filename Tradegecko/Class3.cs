﻿using System;
using System.Collections.Generic;

namespace Tradegecko
{
    class Class3
    {
        public class OrderLineItem
        {
            public int id { get; set; }
            public DateTime created_at { get; set; }
            public DateTime updated_at { get; set; }
            public int order_id { get; set; }
            public int variant_id { get; set; }
            public int tax_type_id { get; set; }
            public string discount { get; set; }
            public object freeform { get; set; }
            public string image_url { get; set; }
            public object label { get; set; }
            public object line_type { get; set; }
            public int position { get; set; }
            public string price { get; set; }
            public string quantity { get; set; }
            public object tax_rate_override { get; set; }
            public object tax_rate { get; set; }
            public IList<int> fulfillment_line_item_ids { get; set; }
            public IList<object> fulfillment_return_line_item_ids { get; set; }
            public IList<int> invoice_line_item_ids { get; set; }
        }

        public class OrderLineItemReponse
        {
            public OrderLineItem order_line_item { get; set; }
        }

    }
}
