﻿using CsvHelper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using WebLibrary;

namespace Tradegecko
{
    public partial class Form1 : Form
    {
        CookieContainer cookieJar = new CookieContainer();

        // AccessToken of my tradegecko account 


        String apiToken = "";
        String authorizeURL = "https://api.tradegecko.com/accounts";
        String orderAPIURL = "https://api.tradegecko.com/orders/";

        String updateOrderAPIURL = "https://api.tradegecko.com/order/";

        String orderLinItenAPIURL = "https://api.tradegecko.com/order_line_items/";
        string variantAPIURL = "https://api.tradegecko.com/variants/";

        string productAPIURL = "https://api.tradegecko.com/products/";


        Class1.APIResponse orders;

        // // // BindingList for DGV
        private BindingList<Class1.Order> dataSource = new BindingList<Class1.Order>();
        private BindingSource bs = new BindingSource();

        public Form1()
        {
            InitializeComponent();

            //  GlobalProxySelection.Select = new WebProxy("127.0.0.1", 8888);
            //  WebRequest.DefaultWebProxy = new WebProxy("127.0.0.1", 8888);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            Boolean allValid = GetUIInputs();

            if (allValid == false)
            {
                MessageBox.Show("Please fill API Token", "Problem", MessageBoxButtons.OK);
                return;
            }

            else
            {
                dgvData.DataSource = null;
                dgvData.Columns.Clear();
                dgvData.Refresh();

                // Authentication from tradegecko      
                HttpWebRequest webReq;
                String response;

                ServicePointManager.CertificatePolicy = new MyCertificate();

                webReq = HTTPHelper.getWebRequestTradegecko(authorizeURL, apiToken);
                response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "GET", "", true);

                // Delete
                webReq = HTTPHelper.getWebRequestTradegecko(orderLinItenAPIURL, apiToken);
                response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "GET", "", true);
                // Delete


                // Getting all orders from gecko
                webReq = HTTPHelper.getWebRequestTradegecko(orderAPIURL, apiToken);
                response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "GET", "", true);

                try
                {
                    orders = JSONHelper.deserializeObject(response);

                    // CSVHelper.writeCSV(orders.orders, true);

                    // Adding Add buttin in start of Row
                    DataGridViewButtonColumn addColumn = new DataGridViewButtonColumn();
                    addColumn.Text = "Add";
                    addColumn.Name = "Add";
                    addColumn.UseColumnTextForButtonValue = true;
                    dgvData.Columns.Add(addColumn);

                    bs = fillBindObjects(dataSource, orders.orders);

                    dgvData.DataSource = bs;
                }

                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        /// <summary>
        /// It collects UI inputs
        /// </summary>
        /// <returns>All Inputs are valid or not</returns>
        private Boolean GetUIInputs()
        {
            Boolean allValid = false;
            try
            {
                if (String.IsNullOrEmpty(txtBxAPIToken.Text) == false)
                {
                    apiToken = txtBxAPIToken.Text;
                    allValid = true;
                }

                else
                {
                    allValid = false;
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }

            return allValid;
        }

        private const string PACKED_STATUS = "unpacked";
        private const string STATUS = "active";

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (orders != null)
            {

                Class1.Order[] _orders = new Class1.Order[orders.orders.Count];
                orders.orders.CopyTo(_orders, 0);

                List<Class1.Order> queryList = new List<Class1.Order>(_orders);

                List<Class1.Order> resultList1 = new List<Class1.Order>();

                resultList1 = queryList.FindAll(x => x.packed_status == PACKED_STATUS && x.status == STATUS);

                dgvData.DataSource = resultList1;
            }
        }

        private void txtBxFileName_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtBxFileName.Text) == false)
            {
                grpBxData.Enabled = true;

                // flag for enabling writing Headers of CSV
                oneTime = true;
            }

            else
            {
                grpBxData.Enabled = false;
            }
        }

        bool oneTime = true;
        private void dgvData_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == dgvData.Columns["Add"].Index && dgvData.Rows.Count > 0) // Clcik event for Edit button
                {
                    Class1.Order selectedOrder = (Class1.Order)dgvData.Rows[e.RowIndex].DataBoundItem;

                    //if (oneTime)
                    //{
                    CSVHelper.writeCSVHeaders(txtBxFileName.Text, true);
                    //    oneTime = false;
                    //}

                    ////// Testing 

                    HttpWebRequest webReq;
                    string response = "";

                    List<VBASheet> vbaSheetList = new List<VBASheet>();

                    for (int i = 0; i < selectedOrder.order_line_item_ids.Count; i++)
                    {
                        webReq = HTTPHelper.getWebRequestTradegecko(orderLinItenAPIURL + selectedOrder.order_line_item_ids[i], apiToken);
                        response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "PUT", "", true);
                        Class3.OrderLineItemReponse orderLineItem = JSONHelper.deserializeOrderLineItem(response);

                        webReq = HTTPHelper.getWebRequestTradegecko(variantAPIURL + orderLineItem.order_line_item.variant_id, apiToken);
                        response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "GET", "", true);
                        Class4.VariantItemReponse variant = JSONHelper.deserializeVariantItem(response);

                        webReq = HTTPHelper.getWebRequestTradegecko(productAPIURL + variant.variant.product_id, apiToken);
                        response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "PUT", "", true);
                        Class5.ProductItemResponse product = JSONHelper.deserializProductItem(response);

                        VBASheet sheet = new VBASheet();
                        sheet.line_number = i + 1;
                        sheet.product_name = product.product.name;
                        sheet.quantity = Double.Parse(orderLineItem.order_line_item.quantity);
                        sheet.attribute_set_value = "";

                        vbaSheetList.Add(sheet);
                    }

                    ////// End Of Testing 

                    CSVHelper.writeCSV(selectedOrder.order_number, vbaSheetList, true);

                    //Once it is saved in CSV, remove it from DataSource

                    bs.RemoveAt(e.RowIndex);
                }
            }

            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        private BindingSource fillBindObjects(BindingList<Class1.Order> bindList, IList<Class1.Order> order)
        {
            BindingSource _bs = new BindingSource();

            bindList.Clear();
            foreach (Class1.Order _order in order)
            {
                bindList.Add(_order);
            }
            _bs.DataSource = null;
            _bs.DataSource = dataSource;

            return _bs;
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            List<Class1.Order> updatedOrders = CSVHelper.readCSV(txtBxFileName.Text);

            HttpWebRequest webReq;
            String response;

            // Authentication from tradegecko  

            //if (updatedOrders.Count > 0)
            //{
            //    webReq = HTTPHelper.getWebRequestTradegecko(authorizeURL, apiToken);
            //    response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "GET", "", true);
            //}

            //for (int i = 0; i < updatedOrders.Count; i++)
            //{
            //    Class1.Order order = updatedOrders[i];

            //    order.packed_status = "packed";
            //    order.status = "finalized";

            //    var obj = order;

            //    dynamic collectionWrapper = new
            //    {
            //        order
            //    };

            //    string orderJson = JSONHelper.serializeOrder(collectionWrapper);

            //    // Updating all orders from gecko   
            //    webReq = HTTPHelper.getWebRequestTradegecko(orderAPIURL + order.id, apiToken);
            //    response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "PUT", orderJson, true);
            //}

            //webReq = HTTPHelper.getWebRequestTradegecko(orderAPIURL + updatedOrders[0].order_line_item_ids[0], apiToken);
            //response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "GET", "", true);


            //   webReq = HTTPHelper.getWebRequestTradegecko(orderAPIURL + orders.orders[0].id, apiToken);
            //   response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "GET", "", true);

            //webReq = HTTPHelper.getWebRequestTradegecko(orderAPIURL + updatedOrders[0].id, apiToken);


            webReq = HTTPHelper.getWebRequestTradegecko(productAPIURL + 7461460, apiToken);
            response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "GET", "", true);

            webReq = HTTPHelper.getWebRequestTradegecko(productAPIURL + 7461460, apiToken);
            response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "PUT", response, true);

            //  https://api.tradegecko.com/orders/{ORDER_ID}

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnInsertNew_Click(object sender, EventArgs e)
        {

            /*
            {
                "accounts": [{
		"id": 39453,
            		"created_at": "2015-12-04T15:14:43.305Z",
            		"updated_at": "2015-12-04T15:14:43.853Z",
            		"contact_email": "ssgcom263@yahoo.com",
            		"contact_mobile": null,
            		"contact_phone": "+923322564682",
            		"country": "PK",
            		"default_order_price_list_id": "wholesale",
            		"default_purchase_order_price_list_id": "buy",
            		"default_sales_ledger_account_on": "company",
            		"default_tax_treatment": "exclusive",
            		"default_document_theme_id": 48531,
            		"industry": "Electronics and IT",
            		"logo_url": null,
            		"name": "herb tech",
            		"stock_level_warn": true,
            		"tax_label": "Tax",
            		"tax_number": null,
            		"tax_number_label": "Tax Number",
            		"time_zone": "Asia/Karachi",
            		"website": null,
            		"primary_location_id": 54900,
            		"primary_billing_location_id": 54900,
            		"default_currency_id": 44665,
            		"default_payment_term_id": 121708,
            		"billing_contact_id": 50659,
            		"default_sales_order_tax_type_id": 92461,
            		"default_purchase_order_tax_type_id": 92462,
            		"default_tax_exempt_id": 92460,
            		"default_tax_rate": "15.0",
            		"default_tax_type": "exclusive",
            		"default_tax_type_id": 92461,
            		"default_order_price_type_id": "wholesale",
            		"default_purchase_order_price_type_id": "buy",
            		"location_ids": [54900],
		"user_ids": [50659]
    }]
}

    */


            // {"errors":{"order_line_items.variant":["can't be blank"]}}

            // ToDo: This is where he needs csv to be uploaded to the web service
            Boolean allValid = GetUIInputs();
            HttpWebRequest webReq;
            String response = null;

            IList<Tradegecko.Class2.PostOrderLineItem> lineItems = new List<Tradegecko.Class2.PostOrderLineItem>();
            int itemIdStart = 12010921; // variant id

            for (int i = 0; i < 2; i++)
            {
                Tradegecko.Class2.PostOrderLineItem itemToAdd = new Class2.PostOrderLineItem();
                itemToAdd.quantity = 1;
                itemToAdd.discount = 0;

                itemToAdd.price = ((i + 3) * 2);
                itemToAdd.tax_rate_override = 0;
                itemToAdd.freeform = false;
                itemToAdd.variant_id = itemIdStart + i;
                lineItems.Add(itemToAdd);
            }
            Tradegecko.Class1.PostAPIOrder anOrder = new Tradegecko.Class1.PostAPIOrder();
            anOrder.billing_address_id = 7108185;       //required // fetch order from api and write its billing address id
            anOrder.shipping_address_id = 7108185;      //required
            anOrder.company_id = 5850050;               //required
            anOrder.email = "test@test.com";            //required
            anOrder.issued_at = "2014-01-10";           //required
            anOrder.tax_type = "exclusive";             //required Must be one of "inclusive" or "exclusive"
            anOrder.fulfillment_status = "unshipped";  //optional: Must be one of "shipped", "partial" or "unshipped"
            //anOrder.order_number = "1234567890123";  //optional
            //anOrder.ship_at = "2014-01-10";            //optional
            anOrder.notes = "test";                    //optional
            anOrder.order_line_items = lineItems; //optional IList<Tradegecko.Class2.OrderLineItem>
            anOrder.payment_status = "unpaid";         //optional Must be one of "paid" or "unpaid"
            anOrder.phone_number = "5555555555";        //optional // any number
            anOrder.reference_number = "987654321";     //optional // any number
            //anOrder.source;                            //optional
            anOrder.status = "active";                   //optional Must be one of "active", "draft" when creating an invoice
            //anOrder.stock_location_id;                 //optional
            //anOrder.source_url;                        //optional

            string orderJson = JSONHelper.serializeOrder(new { order = anOrder });

            webReq = HTTPHelper.getWebRequestTradegecko(orderAPIURL, apiToken);
            response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "POST", orderJson, true);
        }
    }
}

