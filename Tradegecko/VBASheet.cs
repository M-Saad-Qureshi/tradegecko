﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tradegecko
{
    class VBASheet
    {
        public int line_number { get; set; }
        public string product_name { get; set; }
        public double quantity { get; set; }
        public string attribute_set_value { get; set; }
    }
}
