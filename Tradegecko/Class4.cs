﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tradegecko
{
    class Class4
    {
        public class VariantPrice
        {
            public string price_list_id { get; set; }
            public string value { get; set; }
        }

        public class Location
        {
            public int location_id { get; set; }
            public string stock_on_hand { get; set; }
            public string committed { get; set; }
            public object incoming { get; set; }
            public object bin_location { get; set; }
            public object reorder_point { get; set; }
        }

        public class Prices
        {
            public string retail { get; set; }
            public string wholesale { get; set; }
            public string buy { get; set; }
        }

        public class StockLevels
        {
            public string stock_levels { get; set; }
        }

        public class CommittedStockLevels
        {
            public string committed_stock_levels { get; set; }
        }

        public class Variant
        {
            public int id { get; set; }
            public DateTime created_at { get; set; }
            public DateTime updated_at { get; set; }
            public int product_id { get; set; }
            public object default_ledger_account_id { get; set; }
            public string buy_price { get; set; }
            public string committed_stock { get; set; }
            public string incoming_stock { get; set; }
            public object composite { get; set; }
            public object description { get; set; }
            public bool is_online { get; set; }
            public bool keep_selling { get; set; }
            public string last_cost_price { get; set; }
            public bool manage_stock { get; set; }
            public object max_online { get; set; }
            public string moving_average_cost { get; set; }
            public string name { get; set; }
            public bool online_ordering { get; set; }
            public string opt1 { get; set; }
            public object opt2 { get; set; }
            public object opt3 { get; set; }
            public int position { get; set; }
            public string product_name { get; set; }
            public string product_status { get; set; }
            public string product_type { get; set; }
            public string retail_price { get; set; }
            public bool sellable { get; set; }
            public string sku { get; set; }
            public string status { get; set; }
            public string stock_on_hand { get; set; }
            public object supplier_code { get; set; }
            public bool taxable { get; set; }
            public object upc { get; set; }
            public string weight { get; set; }
            public object weight_unit { get; set; }
            public object weight_value { get; set; }
            public string wholesale_price { get; set; }
            public IList<int> image_ids { get; set; }
            public IList<VariantPrice> variant_prices { get; set; }
            public IList<Location> locations { get; set; }
            public Prices prices { get; set; }
            public StockLevels stock_levels { get; set; }
            public CommittedStockLevels committed_stock_levels { get; set; }
        }

        public class VariantItemReponse
        {
            public Variant variant { get; set; }
        }

    }
}
