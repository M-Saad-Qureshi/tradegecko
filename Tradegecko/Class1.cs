﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Tradegecko
{
    class Class1
    {
        public class Invoice
        {
            public int id { get; set; }
            public string invoice_number { get; set; }
        }

        public class InvoiceNumbers
        {
            public string invoice_numbers { get; set; }
        }

        public class Order : PostAPIOrder
        {
            public int id { get; set; }
            public DateTime created_at { get; set; }
            public DateTime updated_at { get; set; }
            public string document_url { get; set; }

            [Browsable(false)]
            public object assignee_id { get; set; }

            [Browsable(false)]
            public object contact_id { get; set; }

            [Browsable(false)]
            public int currency_id { get; set; }

            [Browsable(false)]
            public int user_id { get; set; }

            [Browsable(false)]
            public string default_price_list_id { get; set; }

            public string invoice_status { get; set; }
            public string packed_status { get; set; }
            public string return_status { get; set; }
            public string returning_status { get; set; }
            public string tax_label { get; set; }
            public object tax_override { get; set; }
            public string tax_treatment { get; set; }
            public string total { get; set; }
            public object tags { get; set; }

            [Browsable(false)]
            public IList<int> fulfillment_ids { get; set; }

            [Browsable(false)]
            public IList<object> fulfillment_return_ids { get; set; }

            [Browsable(false)]
            public IList<int> invoice_ids { get; set; }

            [Browsable(false)]
            public IList<object> payment_ids { get; set; }

            [Browsable(false)]
            public IList<object> refund_ids { get; set; }
            [Browsable(false)]
            public IList<Invoice> invoices { get; set; }

            public string cached_total { get; set; }

            [Browsable(false)]
            public string default_price_type_id { get; set; }

            public object tracking_number { get; set; }

            [Browsable(false)]
            public object url { get; set; }

            [Browsable(false)]
            public object source_id { get; set; }

            [Browsable(false)]
            public InvoiceNumbers invoice_numbers { get; set; }

            [Browsable(false)]
            public IList<int> order_line_item_ids { get; set; }

            //[Browsable(false), Bindable(false), 
            //DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), 
            //EditorBrowsable(EditorBrowsableState.Never)]
            //public IList<Tradegecko.Class2.OrderLineItem> order_line_item_ids { get; set; }
        }

        [JsonObject(Title = "Order")]
        public class PostAPIOrder
        {
            [Browsable(false)]
            public int billing_address_id { get; set; } //required

            [Browsable(false)]
            public int shipping_address_id { get; set; } //required

            [Browsable(false)]
            public int company_id { get; set; } //required

            public string email { get; set; } //required
            public string issued_at { get; set; } //required

            public string order_number { get; set; }//optional
            public string ship_at { get; set; } //optional
            public string fulfillment_status { get; set; } //optional: Must be one of "shipped", "partial" or "unshipped"
            public string tax_type { get; set; } //required Must be one of "inclusive" or "exclusive"
            public object notes { get; set; } //optional
            public IList<Tradegecko.Class2.PostOrderLineItem> order_line_items { get; set; } //optional
            public string payment_status { get; set; } //optional Must be one of "paid" or "unpaid"
            public object phone_number { get; set; } //optional

            [Browsable(false)]
            public object reference_number { get; set; } //optional

            [Browsable(false)]
            public object source { get; set; } //optional

            public string status { get; set; } //optional Must be one of "active", "draft" when creating an invoice

            [Browsable(false)]
            public int stock_location_id { get; set; } //optional

            public object source_url { get; set; } //optional
        }

        public class Meta
        {
            public int total { get; set; }
        }

        public class APIResponse
        {
            public IList<Order> orders { get; set; }
            public Meta meta { get; set; }
        }
    }
}
