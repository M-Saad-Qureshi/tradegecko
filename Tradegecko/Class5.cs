﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tradegecko
{
    class Class5
    {
        public class Product
        {
            public int id { get; set; }
            public DateTime created_at { get; set; }
            public DateTime updated_at { get; set; }
            public string brand { get; set; }
            public string description { get; set; }
            public string image_url { get; set; }
            public string name { get; set; }
            public string opt1 { get; set; }
            public object opt2 { get; set; }
            public object opt3 { get; set; }
            public string product_type { get; set; }
            public string quantity { get; set; }
            public string search_cache { get; set; }
            public string status { get; set; }
            public string supplier { get; set; }
            public IList<object> supplier_ids { get; set; }
            public string tags { get; set; }
            public IList<int> variant_ids { get; set; }
            public string vendor { get; set; }
        }

        public class ProductItemResponse
        {
            public Product product { get; set; }
        }
    }
}
